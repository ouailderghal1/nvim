return {
  'tpope/vim-fugitive',
  'tpope/vim-rhubarb',
  'tpope/vim-sleuth',
  'vimwiki/vimwiki',
  'folke/zen-mode.nvim',
  'apple/pkl-neovim',
  'EdenEast/nightfox.nvim',
  'xiyaowong/transparent.nvim',

  { "numToStr/Comment.nvim", opts = {} },
}
